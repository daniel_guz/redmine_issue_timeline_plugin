class IssueTimelineController < ApplicationController
  unloadable


  def show
    @journals = Journal.select(Setting.plugin_issue_timeline.keys).where(journalized_id: params[:issue_id])
  end
end
