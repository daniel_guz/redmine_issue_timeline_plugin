Redmine::Plugin.register :issue_timeline do
  name 'Issue Timeline plugin'
  author 'Daniel Guzz'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'https://bitbucket.org/daniel_guz/redmine_issue_timeline_plugin.git'
  author_url 'https://bitbucket.org/daniel_guz'

  settings :default => {'empty' => true}, :partial => 'settings/issue_timeline_settings'
end
