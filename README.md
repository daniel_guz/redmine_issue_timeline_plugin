# Redmine Issue Timeline Plugin

This is a plugin for Redmine that can display the history of issue at the timeline

# Installation

1. Change into your Redmine installation’s root directory
2. Clone the plugin

    git clone https://bitbucket.org/daniel_guz/redmine_issue_timeline_plugin.git plugins/issue_timeline

3. Delete the .git directory of the plugin and commit it to your favorite version control system

    rm -Rf plugins/issue_timeline/.git
    
4. Restart Redmine instance

# Instructions

1. At the plugin settings choose the fields that should be displayed
2. At the page Issue press Show as timeline
3. The history would be displayed as a timeline